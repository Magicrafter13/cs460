#ifndef CS460_MAIN_H
#define CS460_MAIN_H

#include <pthread.h>
#include <stdlib.h>
#include <sys/time.h>

typedef enum _alg_type AlgType;
enum _alg_type {
	NONE,
	FCFS,
	SJF,
	PR,
	RR
};

typedef enum _thread_status ThreadStatus;
enum _thread_status {
	RUNNING,
	SLEEP,
	FINISH
};

typedef struct _process_data ProcData;
struct _process_data {
	unsigned short priority;
	size_t burst_count;
	unsigned short *burst_time;

	size_t id;

	size_t current_burst;
	float time_remaining;
	enum {
		P_INIT,
		P_READY,
		P_RUNNING,
		P_BLOCKED,
		P_IO,
		P_FINISHED
	} state;

	struct {
		struct timeval spawned, running, ready, entered_queue, finished;
	} time[2];
};

typedef struct _process_list ProcList;
struct _process_list {
	pthread_mutex_t mutex;
	size_t count, size;
	ProcData *list;

	ssize_t running, io;
};

typedef struct _process_queue ProcQueue;
struct _process_queue {
	ProcData *proc;
	ProcQueue *prev, *next;
};

typedef struct _burst_thread_data BurstThreadData;
struct _burst_thread_data {
	char *name;
	ProcQueue **ours, **theirs;
	size_t our_time, their_time;
	AlgType alg;
};

extern struct timeval burst_runtime[2];

extern int quantum;
extern ProcList processes;
extern ProcQueue *cpu_queue, *io_queue;

void *parse_file(void*);
void *burst(void*);

#endif
