#include "main.h"
#ifdef DEBUG
#include <stdio.h>
#endif

void *burst(void *data) {
#ifdef DEBUG
	char *name = ((BurstThreadData*)data)->name;
#endif
	ProcQueue **our_queue = ((BurstThreadData*)data)->ours, **their_queue = ((BurstThreadData*)data)->theirs, *running = NULL, *last, *p;
	size_t our_time = ((BurstThreadData*)data)->our_time, their_time = ((BurstThreadData*)data)->their_time;
	AlgType alg = ((BurstThreadData*)data)->alg;
	double time;
	struct timeval prev, next, start, end, time_asleep, res, other;
	gettimeofday(&start, NULL);
#ifdef DEBUG
	printf("Quantum: %dms\n", quantum);
#endif
	while (processes.list != NULL) {
		pthread_mutex_lock(&processes.mutex);
		while (*our_queue != NULL) {
			// Re-order queue if necessary and get current running process
			running = *our_queue;
			switch (alg) {
				case NONE:
					return NULL;
				case SJF:
					for (p = running->next; p != *our_queue; p = p->next)
						if (p->proc->time_remaining < running->proc->time_remaining)
							running = p;
					/* FALLTHROUGH */
				case FCFS:
					time = running->proc->time_remaining;
					break;
				case PR:
					for (p = running->next; p != *our_queue; p = p->next)
						if (p->proc->priority > running->proc->priority)
							running = p;
					time = running->proc->time_remaining;
					break;
				case RR:
					time = quantum < running->proc->time_remaining ? quantum : running->proc->time_remaining;
					break;
			}
			if (time > 0) {
				last = (*our_queue)->prev; // End of queue before sleep (might not be end after sleep due to IO or new procs)
				// Sleep
#ifdef DEBUG
				printf("%s burst running for process %zu for %f milliseconds.", name, running->proc->id, time);
				for (p = running->next; p != running; p = p->next)
					printf(" Process %zu is waiting.", p->proc->id);
				puts("\033[0m");
#endif
				pthread_mutex_unlock(&processes.mutex);
				gettimeofday(&prev, NULL);
				nanosleep(&(struct timespec){ .tv_sec = 0, .tv_nsec = time * 1000000L }, NULL);
				gettimeofday(&next, NULL);
				// Update running process
				pthread_mutex_lock(&processes.mutex);
				timersub(&next, &prev, &time_asleep);
				running->proc->time_remaining -= time_asleep.tv_sec * 1000 + time_asleep.tv_usec / 1000.0;
				timeradd(&running->proc->time[our_time].running, &time_asleep, &res);
				running->proc->time[our_time].running = res;
				// Update the waiting processes
				p = last;
				do {
					if (p != running) {
						timeradd(&p->proc->time[our_time].ready, &time_asleep, &res);
#ifdef DEBUG
						if (our_time == 0)
							printf("\tProcess %zu waited for %lf milliseconds.\n", p->proc->id, time_asleep.tv_usec / 1000.0);
#endif
						p->proc->time[our_time].ready = res;
					}
					p = p->prev;
				}
				while (p->next != *our_queue);
				// Update any processes that were added while we were waiting
				for (p = last->next; p != *our_queue; p = p->next) {
					timersub(&next, &p->proc->time[our_time].entered_queue, &other);
#ifdef DEBUG
					printf("Process %zu was added to %s queue while running.\033[0m\n", p->proc->id, name);
					if (our_time == 0)
						printf("\tProcess %zu waited for %lf milliseconds.\n", p->proc->id, other.tv_usec / 1000.0);
#endif
					timeradd(&p->proc->time[our_time].ready, &other, &res);
					p->proc->time[our_time].ready = res;
				}
			}
			// This process finished its burst
			if (running->proc->time_remaining <= 0) {
#ifdef DEBUG
				printf("Process finished %s burst for process %zu.\033[0m\n", name, running->proc->id);
#endif
				// Update our queue
				if (*our_queue == (*our_queue)->next)
					*our_queue = NULL;
				else {
					running->prev->next = running->next;
					running->next->prev = running->prev;
					if (*our_queue == running)
						*our_queue = running->next;
				}

				// Update running process
				++running->proc->current_burst;
				running->proc->time[our_time].finished = next;
				// This process is completely finished
				if (running->proc->current_burst == running->proc->burst_count) {
					running->proc->state = P_FINISHED;
					free(running);
				}
				// Otherwise move to their queue
				else {
					running->proc->time_remaining = running->proc->burst_time[running->proc->current_burst];
					running->proc->time[their_time].entered_queue = next;
					if (*their_queue == NULL)
						*their_queue = running->prev = running->next = running;
					else {
						running->next = *their_queue;
						(*their_queue)->prev->next = running;
						running->prev = (*their_queue)->prev;
						(*their_queue)->prev = running;
					}
				}
			}
			else if (alg == RR)
				*our_queue = (*our_queue)->next;
		}
		pthread_mutex_unlock(&processes.mutex);
	}
	gettimeofday(&end, NULL);
	timersub(&end, &start, &burst_runtime[our_time]);
	return NULL;
}
