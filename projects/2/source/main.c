#include "main.h"
#include <errno.h>
#include <stdio.h>
#include <string.h>

#define USAGE(extra_statement) { extra_statement; printf("Usage: %s -alg (FCFS|SJF|PR|RR) [-quantum N] -input FILE\n\tWhere N is an integer followed by 'ms'.\n\tWhere FILE is a filename.\n\n\tQuantum should only be provided when using RR option.\n", argv[0]); return 2; }

pthread_t threads[3];

int main(int argc, char *argv[]) {
	// Not enough arguments
	if (argc < 5)
		USAGE(printf("Not enough arguments.\n\n"));

	AlgType alg = NONE;
	char *filename;

	// Check known args
	if (strcmp(argv[1], "-alg"))
		USAGE(printf("-- Expected: -alg\n--      Got: %s\n\n", argv[1]));
	if (!strcmp(argv[2], "RR"))
		alg = RR;
	else if (!strcmp(argv[2], "FCFS"))
		alg = FCFS;
	else if (!strcmp(argv[2], "SJF"))
		alg = SJF;
	else if (!strcmp(argv[2], "PR"))
		alg = PR;
	switch (alg) {
		case NONE:
			USAGE(printf("-- Expected one of: FCFS, SJF, PR, RR\n--             Got: %s\n\n", argv[2]));
			break;
		// Round-robin
		case RR:
			if (strcmp(argv[3], "-quantum"))
				USAGE(printf("-- Expected: -quantum\n--      Got: %s\n\n", argv[3]));
			int bytes_matched = 0;
			if (sscanf(argv[4], "%dms%n", &quantum, &bytes_matched) < 1 || argv[4][bytes_matched] != '\0')
				USAGE(printf("-- Expected an integer followed by 'ms'\n--      Got %s\n\n", argv[4]));
			if (quantum < 1)
				USAGE(printf("-- Expected an integer greater than 0ms\n--      Got %s\n\n", argv[4]));
			if (strcmp(argv[5], "-input"))
				USAGE(printf("-- Expected: -input\n--      Got: %s\n\n", argv[5]));
			filename = argv[6];
			break;
		// Regular alg
		default:
			if (strcmp(argv[3], "-input"))
				USAGE(printf("-- Expected: -input\n--      Got: %s\n\n", argv[3]));
			filename = argv[4];
	}

	// Attempt to open file
	FILE *file = fopen(filename, "r");
	if (file == NULL) {
		printf("%s: %s", argv[0], strerror(errno));
		return 1;
	}

	processes = (ProcList){
		.list = calloc(16, sizeof (ProcData)),
		.size = 16,
		.running = -1,
		.io = -1
	};
	cpu_queue = io_queue = NULL;

	// Start file parsing thread
	pthread_create(threads, NULL, parse_file, file);
	// Start scheduler
	pthread_create(threads + 1, NULL, burst, &(BurstThreadData){ .name = "\033[1;32mCPU", .ours = &cpu_queue, .theirs = &io_queue, .our_time = 0, .their_time = 1, .alg = alg });
	pthread_create(threads + 2, NULL, burst, &(BurstThreadData){ .name = "\033[1;34mIO", .ours = &io_queue, .theirs = &cpu_queue, .our_time = 1, .their_time = 0, .alg = FCFS });

	ProcData *list = processes.list;
	// Wait for parse thread to stop
	pthread_join(threads[0], NULL);
	fclose(file);
	for (;;) {
		pthread_mutex_lock(&processes.mutex);
		size_t finished = 0;
		for (size_t i = 0; i < processes.count; ++i)
			if (processes.list[i].state == P_FINISHED)
				++finished;
		if (finished == processes.count) {
			processes.list = NULL;
			pthread_mutex_unlock(&processes.mutex);
			break;
		}
		pthread_mutex_unlock(&processes.mutex);
		nanosleep(&(struct timespec){ .tv_sec = 0, .tv_nsec = 10000000 }, NULL); // every 10 ms
	}
#ifdef DEBUG
	fflush(stdout);
	puts("Waiting for burst threads to terminate.");
#endif
	pthread_join(threads[1], NULL);
	pthread_join(threads[2], NULL);

	double turnaround = 0, ready = 0;
	for (size_t i = 0; i < processes.count; ++i) {
		free(list[i].burst_time);
		turnaround += (list[i].time[0].finished.tv_sec - list[i].time[0].spawned.tv_sec) + (list[i].time[0].finished.tv_usec - list[i].time[0].spawned.tv_usec) / 1000000.0;
		ready += list[i].time[0].ready.tv_sec + list[i].time[0].ready.tv_usec / 1000000.0;
	}
	turnaround *= 1000.0 / processes.count;
	ready *= 1000.0 / processes.count;
	free(list);
	// Final output
	printf("Input File Name                 : %s\nCPU Scheduling Alg              : %s", filename, argv[2]);
	if (quantum > 0)
		printf(" (%dms)", quantum);
	printf("\nThroughput                      : %lf\nAvg. Turnaround Time            : %lf\nAvg. Waiting Time in Ready Queue: %lf\n", processes.count / (burst_runtime[0].tv_sec * 1000 + burst_runtime[0].tv_usec / 1000.0), turnaround, ready);
	return 0;
}
