#include "main.h"
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

void *parse_file(void *file) {
	pthread_mutex_lock(&processes.mutex);
	size_t line = 0;
	// For getline
	char *buffer = NULL;
	size_t size = 0;
	ssize_t length = 0;
	// For strtok (most of these strings are redundant and just so we can have nice names...)
	char *cmd_str, *priority_str, *tokens_str, *end_ptr, *time_str;
	ProcData proc;
	while ((length = getline(&buffer, &size, (FILE*)file)) > 0) {
		++line;
		if (buffer[length - 1] == '\n')
			buffer[--length] = '\0';
		cmd_str = strtok(buffer, " ");
		if (!strcmp("proc", cmd_str)) {
			proc = (ProcData){ .state = P_READY };
			if ((priority_str = strtok(NULL, " ")) == NULL) {
				fprintf(stderr, "Unexpected end of line %zu, expected priority integer.\n", line);
				continue;
			}
			if ((tokens_str = strtok(NULL, " ")) == NULL) {
				fprintf(stderr, "Unexpected end of line %zu, expected burst count integer.\n", line);
				continue;
			}
			proc.priority = strtoul(priority_str, &end_ptr, 10);
			if (*end_ptr != '\0') {
				fprintf(stderr, "Unknown argument \"%s\" on line %zu, expected priority integer.\n", priority_str, line);
				continue;
			}
			if (proc.priority > 10 || proc.priority == 0) {
				fprintf(stderr, "Bad integer on line %zu, must be between 1 and 10 (inclusive).\n", line);
				continue;
			}
			proc.burst_count = strtoul(tokens_str, &end_ptr, 10);
			if (*end_ptr != '\0') {
				fprintf(stderr, "Unknown argument \"%s\" on line %zu, expected burst count integer.\n", tokens_str, line);
				continue;
			}
			if (proc.burst_count == 0) {
				fprintf(stderr, "Bad integer on line %zu, must be greater than 0.\n", line);
				continue;
			}
			proc.burst_time = calloc(proc.burst_count, sizeof(unsigned short));
			_Bool error = 0;
			for (size_t i = 0; i < proc.burst_count; ++i) {
				if ((time_str = strtok(NULL, " ")) == NULL) {
					fprintf(stderr, "Unexpected end of line %zu, expected another burst time integer.\n", line);
					error = 1;
					break;
				}
				proc.burst_time[i] = strtoul(time_str, &end_ptr, 10);
				if (*end_ptr != '\0') {
					fprintf(stderr, "Unknown argument \"%s\" on line %zu, expected burst time integer.\n", time_str, line);
					error = 1;
					break;
				}
			}
			proc.time_remaining = proc.burst_time[0];
			if (error) {
				free(proc.burst_time);
				continue;
			}
			// Add process to system
			if (processes.count == processes.size)
				processes.list = reallocarray(processes.list, (processes.size *= 2), sizeof (ProcData));
			proc.id = processes.count;
			processes.list[processes.count] = proc;
			ProcQueue *new_entry = malloc(sizeof (ProcQueue));
			new_entry->proc = &processes.list[processes.count++];
			if (cpu_queue == NULL)
				cpu_queue = new_entry->prev = new_entry->next = new_entry;
			else {
				new_entry->prev = cpu_queue->prev;
				new_entry->next = cpu_queue;
				cpu_queue->prev->next = new_entry;
				cpu_queue->prev = new_entry;
			}
			gettimeofday(&new_entry->proc->time[0].entered_queue, NULL);
			new_entry->proc->time[0].spawned = new_entry->proc->time[1].spawned = new_entry->proc->time[0].entered_queue;
		}
		else if (!strcmp("sleep", cmd_str)) {
			if ((time_str = strtok(NULL, " ")) == NULL) {
				fprintf(stderr, "Unexpected end of line %zu, expected millisecond integer.\n", line);
				continue;
			}
			long time = strtoul(time_str, &end_ptr, 10);
			if (*end_ptr != '\0') {
				fprintf(stderr, "Unknown argument \"%s\" on line %zu, expected sleep millisecond integer.\n", time_str, line);
				continue;
			}
			if (time < 1) {
				fprintf(stderr, "Bad integer on line %zu, must be greater than 0.\n", line);
				continue;
			}
#ifdef DEBUG
			puts("\033[1;31mParse thread going to sleep.\033[0m");
#endif
			pthread_mutex_unlock(&processes.mutex);
			nanosleep(&(struct timespec){ .tv_sec = 0, .tv_nsec = time * 1000000L }, NULL);
#ifdef DEBUG
			puts("\033[1;31mParse thread woke up from sleep, resuming work.\033[0m");
#endif
			pthread_mutex_lock(&processes.mutex);
		}
		else if (!strcmp("stop", cmd_str)) {
			pthread_mutex_unlock(&processes.mutex);
#ifdef DEBUG
			puts("\033[1;31mParse thread exiting.\033[0m");
#endif
			break;
		}
		else
			fprintf(stderr, "Unknown directive \"%s\" on line %zu.\n", cmd_str, line);
	}
	free(buffer);
	return NULL;
}
