#include <dirent.h>
#include <errno.h>
#include <fcntl.h>
#include <pthread.h>
#include <stdatomic.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/sendfile.h>
#include <sys/stat.h>
#include <unistd.h>

int thread_id, files_copied;
unsigned long long bytes_copied;

// Shut up
#define BACKUP "\1"
#define IBACKUP 1
#define RESTORE "\2"
#define IRESTORE 2

typedef struct _backup_data BakData;
struct _backup_data {
	char *src, *dst;
};

void *backup_dir(void *data);

char *program;

int main(int argc, char *argv[]) {
	if (argc > 2) {
		fprintf(stderr, "Too many arguments.\n");
		return 2;
	}
	_Bool backup = 1;
	if (argc == 2) {
		if (strcmp(argv[1], "-r")) {
			fprintf(stderr, "Unrecognized argument `%s'.\n", argv[1]);
			return 2;
		}
		backup = 0;
	}

	program = argv[0];
	pthread_t thread;
	pthread_create(&thread, NULL, backup_dir, strdup(backup ? BACKUP "." : RESTORE "."));
	pthread_join(thread, NULL);

	if (files_copied > 0)
		fprintf(stdout, "Successfully copied %d files (%llu bytes)\n", files_copied, bytes_copied);
}

void *backup_file(void *data) {
	int tid = __atomic_add_fetch(&thread_id, 1, memory_order_relaxed);
	BakData bak = *(BakData*)data;
	//fprintf(stderr, "# Backing up file `%s' to `%s'\n", bak.src, bak.dst);
	int src = open(bak.src, O_RDONLY);
	struct stat buf;
	if (fstat(src, &buf) == 0) {
		int dst = open(bak.dst, O_WRONLY | O_CREAT | O_TRUNC, buf.st_mode);
		off_t size = lseek(src, 0, SEEK_END), total = 0;
		lseek(src, 0, SEEK_SET);
		ssize_t written;
		while (written = sendfile(dst, src, &total, size - total), written > -1 && total != size);
			//fprintf(stderr, "# Wrote %zu bytes to `%s'\n", written, bak.dst);
		if (written == -1)
			fprintf(stderr, "%s: could not write file `%s' from `%s': %s\n", program, bak.dst, bak.src, strerror(errno));
		else {
			fprintf(stdout, "[thread %d] Copied %zu bytes from %s to %s\n", tid, total, bak.src, bak.dst);
			__atomic_add_fetch(&files_copied, 1, memory_order_relaxed);
			__atomic_add_fetch(&bytes_copied, (unsigned long long)total, memory_order_relaxed);
		}
		futimens(dst, (struct timespec[2]){ buf.st_atim, buf.st_mtim });
		close(dst);
	}
	close(src);
	free(bak.dst);
	free(bak.src);
	free(data);
	return NULL;
}

void *backup_dir(void *data) {
	int tid = __atomic_add_fetch(&thread_id, 1, memory_order_relaxed);

	// Get paths
	char *path = &((char*)data)[1];
	//fprintf(stderr, "# Backing up directory `%s'\n", path);
	size_t p_len = strlen(path);
	char backup[p_len + 9];
	memcpy(backup, path, p_len);
	memcpy(&backup[p_len], "/.backup", 9);

	// Open source directory
	DIR *dir = opendir(path);
	if (dir == NULL) {
		fprintf(stderr, "%s: cannot open directory `%s': %s\n", program, path, strerror(errno));
		free(data);
		return NULL;
	}

	// Create backup directory
	if (mkdir(backup, 0755) != 0) {
		if (errno == EEXIST) {
			struct stat buf;
			if (stat(backup, &buf) != 0) {
				fprintf(stderr, "%s: cannot stat directory `%s': %s\n", program, backup, strerror(errno));
				free(data);
				return (void*)1;
			}
			if (!S_ISDIR(buf.st_mode)) {
				fprintf(stderr, "%s: `%s' exists but is not a directory, please rename or delete\n", program, backup);
				free(data);
				return (void*)1;
			}
		}
		else {
			fprintf(stderr, "%s: cannot create directory `%s': %s\n", program, backup, strerror(errno));
			free(data);
			return (void*)1;
		}
	}

	size_t length = 0, size = 16;
	pthread_t *children = calloc(16, sizeof (pthread_t));

	struct dirent *link;
	struct stat buf, b_buf;
	while ((link = readdir(dir)) != NULL) {
		// File path
		size_t d_len = strlen(link->d_name);
		char file_path[p_len + d_len + 3]; // One extra byte in case this is a directory
		memcpy(file_path, path, p_len);
		file_path[p_len] = '/';
		memcpy(&file_path[p_len + 1], link->d_name, d_len + 1);
		// Check file
		if (lstat(file_path, &buf) != 0) {
			fprintf(stderr, "%s: cannot stat file `%s': %s\n", program, file_path, strerror(errno));
			//fprintf(stderr, "# path: %s - d_name: %s\n", path, link->d_name);
			continue;
		}
		if (S_ISDIR(buf.st_mode)) {
			if (!strcmp(link->d_name, ".backup") || !strcmp(link->d_name, ".") || !strcmp(link->d_name, ".."))
				continue;
			// New thread
			if (length == size)
				children = reallocarray(children, size *= 2, sizeof (pthread_t));
			memmove(&file_path[1], file_path, p_len + d_len + 2);
			file_path[0] = ((char*)data)[0];
			pthread_create(&children[length++], NULL, backup_dir, strdup(file_path));
		}
		else if (S_ISREG(buf.st_mode)) {
			// Skip files if restoring
			if (((char*)data)[0] == IRESTORE)
				continue;

			// Backup file
			char backup_path[p_len + d_len + 14];
			memcpy(backup_path, backup, p_len + 8);
			backup_path[p_len + 8] = '/';
			memcpy(&backup_path[p_len + 9], link->d_name, d_len);
			memcpy(&backup_path[p_len + d_len + 9], ".bak", 5);
			//fprintf(stderr, "# Backing up `%s'\n", backup_path);
			fprintf(stdout, "[thread %d] Backing up %s\n", tid, file_path);
			int res = stat(backup_path, &b_buf);
			if (res != 0 && errno != ENOENT)
				fprintf(stderr, "%s: cannot stat file `%s': %s\n", program, backup_path, strerror(errno));
			else {
				_Bool do_backup = 0;
				if (res != 0)
					do_backup = 1;
				else {
					// Current file is newer than backup
					if (buf.st_mtime > b_buf.st_mtime) {
						do_backup = 1;
						//fprintf(stdout, "File `%s' newer than backup, replacing backup version with current version...\n", file_path);
						fprintf(stdout, "[thread %d] WARNING: Overwriting %s\n", tid, backup_path);
					}
					// Current file (possibly) matches backup file
					else if (buf.st_mtime == b_buf.st_mtime)
						//fprintf(stdout, "File `%s' not changed since last backup, skipping...\n", file_path);
						fprintf(stdout, "[thread %d] %s does not need backing up\n", tid, file_path);
					// Current file is older than backup
					else
						//fprintf(stdout, "File `%s' is older than last backup, ignoring...\n", file_path);
						fprintf(stdout, "[thread %d] %s does not need backing up\n", tid, file_path);
				}
				if (do_backup) {
					// New thread
					if (length == size)
						children = reallocarray(children, size *= 2, sizeof (pthread_t));
					//fprintf(stderr, "# About to spawn a thread with src=%s and dst=%s\n", file_path, backup_path);
					BakData *bak = malloc(sizeof (BakData));
					*bak = (BakData){ .src = strdup(file_path), .dst = strdup(backup_path) };
					pthread_create(&children[length++], NULL, backup_file, bak);
				}
			}
		}
	}
	closedir(dir);

	// Now, if restoring iterate over .bak files
	if (((char*)data)[0] == IRESTORE) {
		char backup_path[p_len + 9];
		memcpy(backup_path, path, p_len);
		memcpy(&backup_path[p_len], "/.backup", 9);
		// Open source directory
		DIR *dir = opendir(backup_path);
		if (dir == NULL) {
			if (errno == ENOENT)
				fprintf(stdout, "No .backup directory exists in `%s', skipping...\n", backup_path);
			else
				fprintf(stderr, "%s: cannot open directory `%s': %s\n", program, path, strerror(errno));
		}
		else {
			while ((link = readdir(dir)) != NULL) {
				// File path
				size_t d_len = strlen(link->d_name);
				char file_path[p_len + d_len + 10]; // One extra byte in case this is a directory
				memcpy(file_path, backup_path, p_len + 8);
				file_path[p_len + 8] = '/';
				memcpy(&file_path[p_len + 9], link->d_name, d_len + 1);
				// Check file
				if (lstat(file_path, &buf) != 0) {
					fprintf(stderr, "%s: cannot stat file `%s': %s\n", program, file_path, strerror(errno));
					continue;
				}
				if (S_ISDIR(buf.st_mode))
					continue;

				// Backup file
				char restore_path[p_len + d_len + 2 - 4];
				memcpy(restore_path, path, p_len);
				restore_path[p_len] = '/';
				memcpy(&restore_path[p_len + 1], link->d_name, d_len - 4); // Ignore .bak
				restore_path[p_len + d_len - 3] = '\0';
				//fprintf(stderr, "# Restoring `%s'\n", restore_path);
				fprintf(stdout, "[thread %d] Restoring %s\n", tid, file_path);
				int res = stat(restore_path, &b_buf);
				if (res != 0 && errno != ENOENT)
					fprintf(stderr, "%s: cannot stat file `%s': %s\n", program, restore_path, strerror(errno));
				else {
					_Bool do_restore = 0;
					if (res != 0)
						do_restore = 1;
					else {
						// Current file is newer than backup
						if (buf.st_mtime > b_buf.st_mtime)
							//fprintf(stdout, "File `%s' newer than backup, ignoring...\n", restore_path);
							fprintf(stdout, "[thread %d] %s is already the most current version\n", tid, restore_path);
						// Current file (possibly) matches backup file
						else if (buf.st_mtime == b_buf.st_mtime)
							//fprintf(stdout, "File `%s' not changed since last backup, skipping...\n", restore_path);
							fprintf(stdout, "[thread %d] %s is already the most current version\n", tid, restore_path);
						// Current file is older than backup
						else {
							do_restore = 1;
							fprintf(stdout, "File `%s' older than backup, replacing current version with backup version...\n", restore_path);
						}
					}
					if (do_restore) {
						// New thread
						if (length == size)
							children = reallocarray(children, size *= 2, sizeof (pthread_t));
						//fprintf(stderr, "# About to spawn a thread with src=%s and dst=%s\n", file_path, restore_path);
						BakData *bak = malloc(sizeof (BakData));
						*bak = (BakData){ .src = strdup(file_path), .dst = strdup(restore_path) };
						pthread_create(&children[length++], NULL, backup_file, bak);
					}
				}
			}
		}	
	}

	// Wait for subdirs and files to finish up
	for (size_t i = 0; i < length; ++i)
		pthread_join(children[i], NULL);
	free(children);

	free(data);
	return NULL;
}
