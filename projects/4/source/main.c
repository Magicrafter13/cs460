#include <errno.h>
#include <inttypes.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#define USAGE(extra_statement) { extra_statement; printf("Usage: %s (FIFO|LRU) N FILE\n\tWhere N is the number of physical page frames available.\n\tWhere FILE is the path to a file containing the page-reference sequence.\n", argv[0]); return 2; }

typedef enum _alg_type {
	FIFO,
	LRU,
	CLK,
	OPT
} AlgType;

typedef struct _action {
	_Bool write;
	uint8_t page;
} Action;

typedef struct _frame Frame;
struct _frame {
	_Bool valid, changed, reference;
	uint8_t page;
	unsigned int used;
};

int main(int argc, char *argv[]) {
	// Parse args
	if (argc != 4)
		USAGE(puts("Incorrect number of arguments.\n"));
	AlgType alg;
	if (!strcmp(argv[1], "FIFO"))
		alg = FIFO;
	else if (!strcmp(argv[1], "LRU"))
		alg = LRU;
	else if (!strcmp(argv[1], "CLK"))
		alg = CLK;
	else if (!strcmp(argv[1], "OPT"))
		alg = OPT;
	else
		USAGE(printf("-- Expected one of: FIFO, LRU\n--             Got: %s\n\n", argv[1]));
	uint8_t frames;
	int bytes_matched;
	if (sscanf(argv[2], "%" SCNu8 "%n", &frames, &bytes_matched) < 1 || argv[2][bytes_matched] != '\0')
		USAGE(printf("-- Expected an integer\n--      Got %s\n\n", argv[2]));

	// Attempt to open file
	FILE *file = fopen(argv[3], "r");
	if (file == NULL) {
		printf("%s: cannot open `%s': %s\n", argv[0], argv[3], strerror(errno));
		return 1;
	}

	// Read all action lines
	size_t length = 0, size = 16;
	Action *action = calloc(16, sizeof (Action));
	char buffer[7];
	while (fgets(buffer, 7, file) != NULL) {
		action[length++] = (Action){ .write = buffer[0] == 'W', .page = strtol(&buffer[2], NULL, 10) };
		if (length == size)
			action = reallocarray(action, size *= 2, sizeof (Action));
	}
	fclose(file);

	// Initial TLB allocation
	Frame *tlb = calloc(frames, sizeof (Frame));
	unsigned int misses = 0,
	             writes = 0,
	             t = 1, // Current time slice (system clock)
	             c = 0; // CLK clock hand pointer
	uint8_t f = 0;      // FIFO frame (increment on every miss)

	// Misc data
	uint8_t i; // Index value used for loops
	_Bool miss, write;

#ifdef DEBUG
	for (i = 0; i < frames; ++i)
		fputs("Frm ", stdout);
	puts("Access Miss");
	for (i = 0; i < frames; ++i)
		printf("%3d ", i + 1);
	puts("\n");
	for (i = 0; i < frames; ++i)
		if (tlb[i].valid)
			printf("%3d ", tlb[i].page);
		else
			fputs("    ", stdout);
	fputc('\n', stdout);
#endif

	// Parse each line
	for (size_t a = 0; a < length; ++a) {
		miss = 1, write = 0;
		// Scan TLB for entry
		for (i = 0; i < frames; ++i) {
			if (tlb[i].valid && tlb[i].page == action[a].page) {
				miss = 0;
				tlb[i].reference = 1;
#ifdef DEBUG
				fprintf(stderr, "Update use time for %d to %u\n", action[a].page, t);
#endif
				if (alg != FIFO)
					tlb[i].used = t;
				// If write, replace value
				if (action[a].write) {
#ifdef DEBUG
					fprintf(stderr, "Changed page %d\n", action[a].page);
#endif
					tlb[i].changed = 1;
				}
				break;
			}
		}
		// Determine what to replace (if applicable)
		if (miss) {
#ifdef DEBUG
			fprintf(stderr, "TLB did not have page %d.\n", action[a].page);
#endif
			uint8_t replace;
			switch (alg) {
				case FIFO:
					replace = f++ % frames;
#ifdef DEBUG
					fprintf(stderr, "FIFO replace frame %d, page %d --> %d\n", replace, tlb[replace].page, action[a].page);
#endif
					break;
				case LRU:
					replace = 0;
					for (i = 1; i < frames; ++i)
						if (tlb[i].used < tlb[replace].used)
							replace = i;
#ifdef DEBUG
					fprintf(stderr, "LRU replace frame %d, page %d --> %d\n", replace, tlb[replace].page, action[a].page);
#endif
					break;
				case CLK:
					for (; tlb[c].reference; c = (c + 1) % frames)
						tlb[c].reference = 0;
					replace = c;
					c = (c + 1) % frames;
#ifdef DEBUG
					fprintf(stderr, "CLK replace frame %d, page %d --> %d\n", replace, tlb[replace].page, action[a].page);
#endif
					break;
				case OPT:
					replace = 0;
					size_t next_ref = 0;
					for (i = 0; i < frames; ++i) {
						if (!tlb[i].valid) {
							replace = i;
							break;
						}
						_Bool found = 0;
						for (size_t a2 = a + 1; a2 < length; ++a2) {
							if (action[a2].page == tlb[i].page) {
								found = 1;
								if (a2 > next_ref) {
									replace = i;
									next_ref = a2;
								}
								break;
							}
						}
						if (!found) {
							replace = i;
							break;
						}
					}
#ifdef DEBUG
					fprintf(stderr, "OPT replace frame %d, page %d --> %d\n", replace, tlb[replace].page, action[a].page);
#endif
					break;
			}
			// Write out page if the page being evicted was modified
			if (tlb[replace].changed) {
#ifdef DEBUG
				fprintf(stderr, "Writing out changes before replacement.\n");
#endif
				write = 1;
			}
			// Replace
			tlb[replace] = (Frame){ .valid = 1, .changed = action[a].write, .reference = 1, .page = action[a].page, .used = t };
#ifdef DEBUG
			fprintf(stderr, "Set frame %d to page %d with use time %u\n", replace, action[a].page, t);
#endif
		}

		// Add cost
		if (miss)
			++misses;
		if (write)
			++writes;

#ifdef DEBUG
		for (i = 0; i < frames; ++i)
			if (tlb[i].valid)
				printf("%3d ", tlb[i].page);
			else
				fputs("    ", stdout);
		printf("%4d    ", action[a].page);
		if (miss)
			fputc('X', stdout);
		fputc('\n', stdout);
		/*for (i = 0; i < frames; ++i)
			printf(" %c%c ", c == i ? 'C' : ' ', tlb[i].reference ? '1' : '0');
		fputc('\n', stdout);*/
#endif

		++t;
	}

	// Final output
	printf(" Page references: %u\n Page misses:     %u\nTotal miss time:  %u\nTotal write time: %u\n", t - 1, misses, misses * 5 + writes * 10, writes * 10);
	
	free(tlb);
	free(action);
}
